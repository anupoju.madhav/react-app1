import React, { useState } from 'react';
import Expenses from './components/Expenses/Expenses';
import NewExpense from './components/NewExpense/NewExpense';

const PRELOADED = [
  {
    id: 'e1',
    title: 'Bike service',
    amount: 900,
    date: new Date(2021, 7, 20)
  },
  {
    id: 'e2',
    title: 'ABC insurance',
    amount: 50000,
    date: new Date(2021, 7, 17)
  },
];
// function App() {
  const App=()=>{

  const [expenses, setExpenses] = useState(PRELOADED);


  const addExpenseHandler = (expense) => {
    // setExpenses([expense, ...expenses]);

    setExpenses((prevExpenses)=>{
      return [expense, ...prevExpenses]
    });
    console.log('In App.js');
    console.log(expense)
  };
  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenses} />

    </div>

  );
}

export default App;
