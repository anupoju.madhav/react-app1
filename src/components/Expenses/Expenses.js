import React, { useState } from 'react';
// import ExpenseItem from './ExpenseItem';
import '../css/ExpenseItem.css'
import '../css/Expenses.css'
import Card from '../UI/Card';
import ExpenseFilter from './ExpenseFilter';
import ExepensesList from './ExepensesList'
// function Expenses(props) {
const Expenses = (props) => {
    const [filteredYear, setFilteredYear] = useState('2021');

    const filterChangeHandler = (selectedyear) => {
        console.log('Expenses.js');
        console.log(selectedyear);
        setFilteredYear(selectedyear)
    }

    const filteredExpenses = props.items.filter(expense => {
        return expense.date.getFullYear().toString() === filteredYear;
    });
    
    return (
        <div>
            <Card className="expenses">
                <ExpenseFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />
               <ExepensesList items={filteredExpenses}/>
            </Card>
        </div>
    );
};

export default Expenses;