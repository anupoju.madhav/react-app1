import React, { useState } from 'react';
import '../css/ExpenseItem.css'
import Card from '../UI/Card';
import './ExpenseDate'
import ExpenseDate from './ExpenseDate';
// function ExpenseItem(props) {
const ExpenseItem = (props) => {
    // useState(props.title);

    const [title, setTitle] =useState(props.title);
    console.log("Expenses item is evaluated by React");
    // let title = props.title;
    const clickHandler = () => {
        setTitle('Updated!')
        console.log(title);

    }

    return (
        <li>
        <Card className="expense-item">
            <ExpenseDate date={props.date} />

            <div className="expense-item__description">
                <h2>{props.title}</h2>
                <div className="expense-item__price">Rs: {props.amount}</div>
            </div>
            <button onClick={clickHandler}>Change Title</button>
        </Card>
        </li>
    );
}

export default ExpenseItem